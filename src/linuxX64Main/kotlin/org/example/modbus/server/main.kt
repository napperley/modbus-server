@file:Suppress("EXPERIMENTAL_UNSIGNED_LITERALS", "EXPERIMENTAL_API_USAGE")

package org.example.modbus.server

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.system.exitProcess

fun main(args: Array<String>): Unit = runBlocking {
    validateProgramArguments(args)
    // Extract the server host, and port from the program arguments (eg 127.0.0.1 1502).
    val host = args.first()
    val port = args.last().toInt()
    launch { connectionsCsp() }
    startServer(host, port)
}

private fun validateProgramArguments(args: Array<String>) {
    val reqArgSize = 2

    @Suppress("RedundantIf")
    val valid = if (args.size != reqArgSize) false
    else if (!args.last().containsInt()) false
    else true
    if (!valid) {
        printUsage()
        exitProcess(-1)
    }
}

private fun printUsage() {
    println("""
        ---- Modbus Server Usage ----
        modbus_server <host> <port> (eg modbus_server 127.0.0.1 10000)
    """.trimIndent())
}

val Boolean.intValue: Int
    get() = if (this) 1 else 0

fun String.containsInt() = try {
    toInt()
    true
} catch (ex: NumberFormatException) {
    false
}