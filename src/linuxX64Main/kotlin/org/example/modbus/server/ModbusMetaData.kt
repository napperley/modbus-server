package org.example.modbus.server

@Suppress("EXPERIMENTAL_UNSIGNED_LITERALS")
/**
 * Contains the Modbus meta data used by the Modbus server. The binary values are based on
 * [this file](https://github.com/stephane/libmodbus/blob/master/tests/unit-test.h.in)
 */
internal object ModbusMetaData {
    const val NODE_ADDR = 3
    const val TOTAL_REGISTERS = 100
    const val READ_REGISTER_ADDR = 40001u
    const val WRITE_HOLDING_REGISTER_ADDR = 60001u
    val registerAddresses = arrayOf(READ_REGISTER_ADDR, WRITE_HOLDING_REGISTER_ADDR)
}
