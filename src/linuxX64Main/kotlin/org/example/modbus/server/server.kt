@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.modbus.server

import kotlinx.cinterop.*
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.delay
import modbus.*
import platform.posix.*
import kotlin.system.exitProcess

private const val DEBUG_ENABLED = true
private const val MAX_CONNECTIONS = 5u
private val functionTypes = arrayOf(READ_INPUT_REGISTERS, WRITE_SINGLE_HOLDING_REGISTER)
private val modbusMapping by lazy { createModbusMapping() }

internal suspend fun startServer(host: String, port: Int) {
    val errorCode = -1
    println("Starting Modbus server (on $host:$port)...")
    val modbusCtx = createModbusCtx(host, port)
    checkModbusMapping(modbusCtx)
    val tcpFd = modbus_tcp_listen(ctx = modbusCtx, nb_connection = MAX_CONNECTIONS.toInt())
    if (tcpFd == errorCode) {
        println("Cannot open TCP port.")
        exitProcess(-1)
    }
    setupRegisters()
    processConnections(modbusCtx, tcpFd)
}

private suspend fun processConnections(modbusCtx: CPointer<modbus_t>?, tcpFd: Int) {
    while (true) {
        // Minimise CPU usage with looping.
        delay(50L)
        val totalConnections = CompletableDeferred<UInt>()
        connectionsChannel.send(ConnectionsMessage.Total(totalConnections))
        if (totalConnections.await() < MAX_CONNECTIONS) {
            println("Waiting for a connection...")
            // This next line will block program execution until there is a connection made.
            modbus_tcp_accept(modbusCtx, cValuesOf(tcpFd))
            connectionsChannel.send(ConnectionsMessage.Increment)
            runEventLoop(modbusCtx, modbusMapping)
        }
    }
}

private fun createModbusCtx(host: String, port: Int): CPointer<modbus_t>? {
    val result = modbus_new_tcp(host, port)
    modbus_set_indication_timeout(ctx = result, to_sec = 3u, to_usec = 0u)
    modbus_set_debug(result, DEBUG_ENABLED.intValue)
    return result
}

/** Allocates memory for input, and holding registers. */
private fun createModbusMapping() = modbus_mapping_new(
    nb_registers = ModbusMetaData.TOTAL_REGISTERS,
    nb_bits = 0,
    nb_input_registers = ModbusMetaData.TOTAL_REGISTERS,
    nb_input_bits = 0
)

private fun setupRegisters() {
    val tmp = modbusMapping?.pointed
    if (tmp != null) {
        setupInputRegisters(tmp)
        setupHoldingRegisters(tmp)
    }
}

private fun setupInputRegisters(modbusMapping: modbus_mapping_t) {
    println("Setting up input registers...")
    modbusMapping.tab_input_registers?.set(0, (rand() % 1000).toUShort())
    modbusMapping.tab_input_registers?.set(1, (rand() % 100).toUShort())
    modbusMapping.tab_input_registers?.set(2, 333u.toUShort())
    modbusMapping.tab_input_registers?.set(3, 70u.toUShort())
}

private fun setupHoldingRegisters(modbusMapping: modbus_mapping_t) {
    println("Setting up holding registers...")
    val initValue = 0u.toUShort()
    modbusMapping.tab_registers?.set(0, initValue)
    modbusMapping.tab_registers?.set(1, initValue)
    modbusMapping.tab_registers?.set(2, initValue)
    modbusMapping.tab_registers?.set(3, initValue)
}

private fun cleanUp(modbusCtx: CPointer<modbus_t>?, modbusMapping: CPointer<modbus_mapping_t>?, tcpFd: Int = -1) {
    if (tcpFd != -1) close(tcpFd)
    modbus_mapping_free(modbusMapping)
    modbus_close(modbusCtx)
    modbus_free(modbusCtx)
}

@Suppress("ReplaceRangeToWithUntil")
private suspend fun runEventLoop(modbusCtx: CPointer<modbus_t>?,
                                 modbusMapping: CPointer<modbus_mapping_t>?) = memScoped {
    val errorCode = -1
    val headerLength = modbus_get_header_length(modbusCtx)
    var receiveRc: Int
    val query = allocArray<uint8_tVar>(MODBUS_TCP_MAX_ADU_LENGTH)
    while (true) {
        // Minimise CPU usage with looping.
        delay(50L)
        do {
            receiveRc = modbus_receive(modbusCtx, query)
            // Filtered queries return 0.
        } while (receiveRc == 0)

        if (receiveRc == errorCode && errno == CONN_RESET_CODE) {
            println("Connection reset by client.")
            connectionsChannel.send(ConnectionsMessage.Decrement)
            break
        } else if (receiveRc == errorCode && errno != EMBBADCRC) {
            connectionsChannel.send(ConnectionsMessage.Decrement)
            break
        }
        processRequest(query = query, headerLength = headerLength, modbusCtx = modbusCtx, modbusMapping = modbusMapping,
            reqLength = receiveRc)
    }
}

private fun processRequest(
    query: CArrayPointer<uint8_tVar>,
    headerLength: Int,
    modbusCtx: CPointer<modbus_t>?,
    modbusMapping: CPointer<modbus_mapping_t>?,
    reqLength: Int
) {
    val registerAddr = extractRegisterAddress(query, headerLength)
    val data = extractData(query, headerLength)
    val functionType = extractFunctionType(query)
    println("Header Length: $headerLength")
    println("Function Type: $functionType")
    println("Register Address: $registerAddr")
    println("Data: $data")
    if (validateRequest(query = query, modbusCtx = modbusCtx, headerLength = headerLength)) {
        if (functionType == WRITE_SINGLE_HOLDING_REGISTER) changeHoldingRegister(registerAddr, data.toUShort())
        println("Sending reply...")
        modbus_reply(ctx = modbusCtx, req = query, req_length = reqLength, mb_mapping = modbusMapping)
    }
}

private fun extractData(query: CArrayPointer<uint8_tVar>, headerLength: Int) =
    getInt16FromInt8(query, headerLength + 3)

private fun extractRegisterAddress(query: CArrayPointer<uint8_tVar>, headerLength: Int) =
    getInt16FromInt8(query, headerLength + 1)

private fun changeHoldingRegister(registerAddress: Int, value: UShort) {
    modbusMapping?.pointed?.tab_registers?.set(registerAddress, value)
}

private fun validateRequest(query: CArrayPointer<uint8_tVar>, modbusCtx: CPointer<modbus_t>?,
                            @Suppress("UNUSED_PARAMETER") headerLength: Int): Boolean {
    var result = true
    @Suppress("CascadeIf")
    if (extractNodeAddress(query) != ModbusMetaData.NODE_ADDR) {
        result = false
        modbus_reply_exception(ctx = modbusCtx, req = query, exception_code = MODBUS_EXCEPTION_GATEWAY_TARGET)
    } else if (extractFunctionType(query) !in functionTypes) {
        result = false
        modbus_reply_exception(ctx = modbusCtx, req = query, exception_code = MODBUS_EXCEPTION_ILLEGAL_FUNCTION)
    }
    // TODO: Figure out how to handle the register address.
    //else if (getInt16FromInt8(query, headerLength + 1).toUInt() != ModbusBytes.REGISTER_ADDR) {
//        result = false
//        modbus_reply_exception(ctx = modbusCtx, req = query, exception_code = MODBUS_EXCEPTION_ILLEGAL_DATA_ADDRESS)
//    }
    return result
}

private fun extractNodeAddress(query: CArrayPointer<uint8_tVar>) = query[6].toInt()

private fun extractFunctionType(query: CArrayPointer<uint8_tVar>) = query[7].toInt()

private fun checkModbusMapping(modbusCtx: CPointer<modbus_t>?) {
    if (modbusMapping == null) {
        fprintf(stderr, "Failed to allocate the mapping: %s\n", strerror(errno))
        println("Exiting...")
        cleanUp(modbusCtx, modbusMapping)
        exitProcess(-1)
    }
}