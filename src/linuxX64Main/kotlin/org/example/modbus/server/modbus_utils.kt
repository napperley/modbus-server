@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.example.modbus.server

import kotlinx.cinterop.CArrayPointer
import kotlinx.cinterop.get
import platform.posix.uint8_tVar

internal const val READ_INPUT_REGISTERS = 0x04
internal const val WRITE_SINGLE_HOLDING_REGISTER = 0x06
internal const val CONN_RESET_CODE = 104

internal fun getInt16FromInt8(tabInt16: CArrayPointer<uint8_tVar>, index: Int): Int {
    val value1 = tabInt16[index].toInt()
    val value2 = tabInt16[index + 1].toInt()
    return (value1 shl 8) or value2
}
