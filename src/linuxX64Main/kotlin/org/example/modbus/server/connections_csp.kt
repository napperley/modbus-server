@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.modbus.server

import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.selects.select

private var connections = 0u
internal val connectionsChannel = Channel<ConnectionsMessage>()

internal sealed class ConnectionsMessage {
    object Increment : ConnectionsMessage()

    object Decrement : ConnectionsMessage()

    data class Total(val result: CompletableDeferred<UInt>) : ConnectionsMessage()
}

internal suspend fun connectionsCsp() {
    while (true) {
        delay(50L)
        select<Unit> {
            connectionsChannel.onReceive { msg ->
                when (msg) {
                    is ConnectionsMessage.Increment -> connections++
                    is ConnectionsMessage.Decrement -> connections--
                    is ConnectionsMessage.Total -> msg.result.complete(connections)
                }
            }
        }
    }
}