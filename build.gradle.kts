group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.3.71"
}

repositories {
    jcenter()
    mavenCentral()
}

kotlin {
    val programArgs = extractProgramArguments()
    linuxX64 {
        compilations.getByName("main") {
            dependencies {
                val coroutinesVer = "1.3.5"
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-native:$coroutinesVer")
            }
            cinterops.create("modbus") {
                val modbusDir = "${System.getProperty("user.home")}/libmodbus-3.1.6"
                includeDirs("$modbusDir/include/modbus")
                // TODO: Find a way to set the library path for the linker in this build file instead of hard coding
                //  the absolute path in the modbus.def file.
                linkerOpts("-L$modbusDir/lib", "-lmodbus")
            }
        }
        binaries {
            executable("modbus_server") {
                entryPoint = "org.example.modbus.server.main"
                runTask?.args(*programArgs)
            }
        }
    }
}

fun extractProgramArguments(): Array<String> {
    val tmp = mutableListOf<String>()
    file("program_args.txt").forEachLine { tmp += it }
    return tmp.toTypedArray()
}